<!---
# SPDX-FileCopyrightText: © 2023 Christian BUHTZ <c.buhtz@posteo.jp>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# This file is part of the program "Hyperorg" which is released under GNU
# General Public License v3 (GPLv3).
# See folder LICENSES or go to <https://www.gnu.org/licenses/#GPL>.
-->
# Hyperorg - An Org to HTML converter

_Hyperorg_ converts files from
[GNU Emacs's](https://www.gnu.org/software/emacs/)
[Org Mode](https://orgmode.org/) and [Org-roam](https://www.orgroam.com/) into
[HTML](https://en.wikipedia.org/wiki/HTML) format. The resulted HTML files are
intended for serverless local use. The creation of _Hyperorg_ was driven by the
experience that there is no known solution available to reliably handle the
*ID-Links* in *Org-roam v2* nodes. See the [example](#example) and read the
sections about
[benefits compared to similar tools](#benefits-compared-to-similar-tools) and
[usage](#usage) for further details.

### Status of the project

It is active and in constant
development implementing
[new features](https://codeberg.org/buhtz/hyperorg/issues?q=&state=open&labels=40047)
and fixing
[bugs](https://codeberg.org/buhtz/hyperorg/issues?q=&state=open&labels=40046).
See [milestones](https://codeberg.org/buhtz/hyperorg/milestones) about what
is coming and the
[changelog](https://codeberg.org/buhtz/hyperorg/src/branch/latest/CHANGELOG.md)
about what has been.
Don't hesitate to voice your wishes and opinions by utilizing the
[issue section](https://codeberg.org/buhtz/hyperorg/issues).

<sub>March 2024</sub>

### Technologies & Standards

 - [Python 3](https://python.org)
 - [`orgparse`](https://orgparse.readthedocs.io/en/latest/)
 - [W3C](https://www.w3.org/) conform
   [HTML5](https://wikipedia.org/wiki/HTML5)
   & [CSS](https://wikipedia.org/wiki/CSS)
 - [Semantic Versioning](https://semver.org),
   [Common Changelog](https://common-changelog.org) and
   [Conventional Commits](https://www.conventionalcommits.org)
 - [REUSE Software](https://reuse.software) and
   [SPDX](https://spdx.github.io/spdx-spec) ([ISO/IEC 5962:2021](https://www.iso.org/obp/ui/en/#iso:std:iso-iec:5962)) specifications

## Table of contents
- [Installation](#installation)
- [Usage](#usage)
- [Benefits compared to similar tools](#benefits-compared-to-similar-tools)
- [How to ask questions, report bugs or suggest new features?](#how-to-ask-questions-report-bugs-or-suggest-new-features)
- [Donations](#donations)
- [Contributing](#contributing)
- [Example](#example)

# Installation
See the [Releases](https://codeberg.org/buhtz/hyperorg/releases) section or
run the following command to install the _latest stable release_ from upstream
repository:

```
$ pipx install https://codeberg.org/buhtz/hyperorg/archive/v0.1.0.zip
```

If `pipx` is not available on your system check your GNU/Linux distributions
package repository or install it from PyPi (`pip install pipx`).

_Hyperorg_ is [not yet available](https://repology.org/project/hyperorg) in a
GNU Linux distribution and there are no plans to release it on PyPi.

# Usage

```
$ hyperorg ~/orgfolder ~/htmlfolder
```

This will take all `*.org` files in the folder `~/orgfolder` and store them
converted to HTML in `~/htmlfolder`. Open the `index.html` file in the
output folder to see the result.

These are all available options:
```
usage: hyperorg [-h] [--hardlinks] [-s] [-v] [-d] [--version] inputdir outputdir

positional arguments:
  inputdir       Source directory with org-files.
  outputdir      Destination directory for html-files.

optional arguments:
  -h, --help     show this help message and exit
  --hardlinks    Use hardlinks instead of symlinks for images and other attachments in the input directory.
  -s, --show     Open result in default browser.
  -v, --verbose  Give detailed information.
  -d, --debug    Debug output.
  --version      show program's version number and exit
```

# Benefits compared to similar tools

- Functions out of the box, no configuration needed.
- Operates as a standalone application, eliminating the requirement for Emacs.
- Fairly resilient when dealing with parser issues.
- Fairly resilient managing dead and problematic links which are a common
  phenomenon when working with a constantly evolving Zettelkasten or personal
  wiki.
- Generates a comprehensive index of all nodes.
- Adhers to World Wide Web Consortium (W3C) standards for HTML5 and CSS
  (`<!DOCTYPE html>`).

The reason alternative tools may not meet these criteria could also stem from
the author of _Hyperorg_ encountering difficulties in comprehending or managing
these functionalities, as well as their documentation. However, this does not
necessarily imply that alternative tools do not actually provide these
functions.

# How to ask questions, report bugs or suggest new features?

The project is in its nascent stage and currently has a modest user base. Any
issues, questions, or discussions can be directed to the
[Issues](https://codeberg.org/buhtz/hyperorg/issues) section. In addition
reporting _bugs_ and _requesting features_, this section accommodates all
inquiries and conversations related to the project's development and usage.

# Donations

Currently, development of _Hyperorg_ is driven by one individual in their spare
time. The infrastructure being used does not incur any costs for this
project. Consequently, small donations do not significantly contribute to the
project's progress. To financially compensate the developer's time, donations
would need to be at a level that is currently not anticipated. Therefore, it is
recommended to financially support projects and software that are relevant and
essential for the infrastructure and further development of _Hyperorg_.

- [Codeberg](https://docs.codeberg.org/improving-codeberg/donate/) for hosting
  that project. They use [Forgejo](https://forgejo.org/) on their servers and
  are involved in its development.
- [Debian GNU/Linux](https://www.debian.org/donations.en.html)
- [GNU Emacs](https://www.gnu.org/software/emacs/) can indirect be supported
  via donating to the [Free Software Foundation](https://my.fsf.org/donate/).
- [Org-mode](https://liberapay.com/org-mode)
- [Org-roam](https://www.orgroam.com/) currently accept donations only via
  [Microsoft GitHub Sponsors](https://github.com/sponsors/jethrokuan).
- [Regex101.com](https://regex101.com) (see _Donate_ button in the top right)
- Any other project or institution relevant for Free and Open-Source
  Software (FOSS).

# Contributing
Please see [CONTRIBUTING](CONTRIBUTING.md) for details about how to contribute
to the project.

The following communities have supported this project with contributions to
discussions, tips, other valuable contributions and also inspirations.

- The communities around [Emacs](https://www.gnu.org/software/emacs/),
  [Org-roam](https://www.orgroam.com/),
  [Org-mode](https://orgmode.org/) and
  [ox-hugo](https://ox-hugo.scripter.co/).
- [Regex101](https://regex101.com)
- [StackOverflow](https://stackoverflow.com)
- [debianforum.de](https://debianforum.de)

# Example

On the left side of this screenshot, an _Emac's_ window displaying an _Org_
buffer can be seen, while on the right side, the same content is prestend in
_HTML_ format.  ![Example Screenshot](misc/screenshot01.png)

Here you can observe the identical content presented in both raw _Org_ and
_HTML_ formats.

<table>
<tr>
<th style="vertical-align:top;width:50%">Org</th>
<th style="vertical-align:top;width:50%">HTML</th>
</tr>
<tr>
<td>

```Markdown
:PROPERTIES:
:ID:       13ec1e89-abc9-4d43-a4a4-3005a1c9dfc7
:END:
#+title: Foo
#+date: [2022-03-09 Mi 09:30]
* Introduction
This node is named "foo" and should link to "bar".
This is the link: [[id:e4bd446b-216b-4e34-8d10-428b6fa5e257][bar]]

* H1 Heading
** H2 Heading
Governments of the Industrial World, you weary giants of flesh and steel, I
come from Cyberspace, the new home of Mind. On behalf of the future, I ask
you of the past to leave us alone.

- You are not welcome among us.
- You have no sovereignty where we gather.

We have no elected government, nor are we likely to have one, so I address
you with no greater authority than that with which liberty itself always
speaks.

1. I declare the global social space we are building to be naturally
   independent
2. of the tyrannies you seek to impose on us.

Source: [[https://www.eff.org/de/cyberspace-independence]]

** A https link

[[https://codeberg.org/buhtz/hyperorg][The "hyperorg" repository]]

* About backlinks
See the autogenerated backlinks in the next section. The node "bar" does link
to this node "foo".
```

</td>
<td>

```html
<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="generator" content="hyperorg" />
    <meta name="generator_version" content="0.1.0" />
    <meta name="generator_website" content="https://codeberg.org/buhtz/hyperorg" />
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="pygments.css" />
    <title>Foo</title>
</head>
<body>
    <nav>
            <ul>
                <li><a href="index.html">Index</a> (1013)</li>
                <li><a href="index__filetag__no_tags_.html">(no tags)</a> (514)</li>
                <li><a href="index__filetag_Bib.html">Bib</a> (277)</li>
                <li><a href="index__filetag_Wiki.html">Wiki</a> (166)</li>
                <li><a href="index__filetag_Meta.html">Meta</a> (43)</li>
                <li><a href="index__filetag_Project.html">Project</a> (19)</li>
            </ul></nav>
    <main>
        <h1>Foo</h1>
        <section>
            <header><div><span class="label">date:</span>&nbsp;[2022-03-09 Mi 09:30]</div>            </header>
<h2>Introduction</h2>
<p>This node is named &quot;foo&quot; and should link to &quot;bar&quot;. This is the link: <a href="20240319084651-bar.html">bar</a></p>
<h2>H1 Heading</h2>
<h3>H2 Heading</h3>
<p>Governments of the Industrial World, you weary giants of flesh and steel, I come from Cyberspace, the new home of Mind. On behalf of the future, I ask you of the past to leave us alone.</p>
<ul>
<li>You are not welcome among us.</li>
<li>You have no sovereignty where we gather.</li>
</ul>
<p>We have no elected government, nor are we likely to have one, so I address you with no greater authority than that with which liberty itself always speaks.</p>
<ol>
<li>I declare the global social space we are building to be naturally independent</li>
<li>of the tyrannies you seek to impose on us.</li>
</ol>
<p>Source: <a target="_blank" rel="noopener noreferrer" href="https://www.eff.org/de/cyberspace-independence">https://www.eff.org/de/cyberspace-independence</a></p>
<h3>A https link</h3>
<p><a target="_blank" rel="noopener noreferrer" href="https://codeberg.org/buhtz/hyperorg">The &quot;hyperorg&quot; repository</a></p>
<h2>About backlinks</h2>
<p>See the autogenerated backlinks in the next section. The node &quot;bar&quot; does link to this node &quot;foo&quot;.</p>
        </section>
        <section>
            <hr />
            <h2>Backlinks</h2>
<ul>
<li><a href="20240319084651-bar.html">bar</a></li>
</ul>
        </section>
    </main>
    <footer>
        Generated with <a href="https://codeberg.org/buhtz/hyperorg">hyperorg</a> 0.1.0 on Di 19 Mär 2024 09:03:51
    </footer>
</body>
</html>
```

</td>
</tr>
</table>

