<!---
# SPDX-FileCopyrightText: © 2024 Christian BUHTZ <c.buhtz@posteo.jp>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# This file is part of the program "Hyperorg" which is released under GNU
# General Public License v3 (GPLv3).
# See folder LICENSES or go to <https://www.gnu.org/licenses/#GPL>.
-->
# Example
The folder `org` contain Org Roam nodes and pictures as attachments. To
generate HTML of it use the following command.

    $ hyperorg org html

The result can be found in the folder `html`. It is recommended to start with
opening the file `index.html` then.
