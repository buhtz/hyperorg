# SPDX-FileCopyrightText: © 2024 Christian BUHTZ <c.buhtz@posteo.jp>
#
# SPDX-License-Identifier: CC0-1.0
#
# This file is part of the program "Hyperorg". Unlike the program, this file is
# licensed under Creative Commons Zero v1 (CC0-1.0). See folder LICENSES or go
# to <https://creativecommons.org/publicdomain/zero/1.0/>
:PROPERTIES:
:ID:       775e781b-bad5-4b0f-ac13-10f168ef789b
:END:
#+title: Mercury
#+date: [2024-06-11 21:55]
- First planet in the [[id:55c55f73-9f29-49ad-a7b9-58a1b6757165][Solar System]]
- Smallest in the Solar System

More facts (using description list):

- Diameter :: approx. 4,880 km
- Mass :: approx. 3.301 × 10^23 kg (0.055 Earth masses)
- Age :: approx. 4.5 billion years
- Surface Temperature :: -173 °C to 427 °C
- Orbital Period :: approx. 88 days
- Rotational Period :: approx. 59 days
- Distance to the Sun :: approx. 57.9 million km (0.39 Astronomical Units)
              
