# SPDX-FileCopyrightText: © 2023 Christian BUHTZ <c.buhtz@posteo.jp>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# This file is part of the program "Hyperorg" which is released under GNU
# General Public License v3 (GPLv3).
# See folder LICENSES or go to <https://www.gnu.org/licenses/#GPL>.
"""Test about the hyperorg package (__main__.py)"""
import unittest
import subprocess
import hyperorg
import hyperorg.__main__


class TestHyperorg(unittest.TestCase):
    """Tests for __main__.py and __init__.py"""

    def _get_git_branch(self):
        return subprocess.check_output(
            ['git', 'symbolic-ref', '--short', 'HEAD']) \
            .decode().replace('\n', '')

    def _get_git_commit_hash(self):
        return subprocess.check_output(
            ['git', 'log', '-n', '1', '--format=%H']) \
            .decode().replace('\n', '')

    def test_git_repository_info(self):
        """Git version infos."""
        self.assertEqual(
            {
                'branch': self._get_git_branch(),
                'hash': self._get_git_commit_hash()[:8],
            },
            hyperorg.get_git_repository_info())

    def test_simplify(self):
        """Remove unusual characters from a string"""
        self.assertEqual(
            hyperorg.simplify_string('<>()ads :-fäÜÖß'),
            '____ads__-fäÜÖß'
        )
