# SPDX-FileCopyrightText: © 2023 Christian BUHTZ <c.buhtz@posteo.jp>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# This file is part of the program "Hyperorg" which is released under GNU
# General Public License v3 (GPLv3).
# See folder LICENSES or go to <https://www.gnu.org/licenses/#GPL>.
"""The content module offering the Node class."""
from __future__ import annotations
import logging
import pathlib
from collections import defaultdict
from typing import Hashable, Iterable, Dict
from hyperorg import elements
from hyperorg import textruns
from hyperorg import parser

_log = logging.getLogger(__name__)


class Node:
    """Org-file body in a generalized and strcutured format.

    The body of an org-file with all its elements (paragraphs, lists, ...) and
    their structure is represented by an object of that clase. On base of one
    of its objects it can easiely transformed to another file-format
    (e.g. HTML). An object Content is the input for the Exporter class.

    The Org syntax reference can be found at
    https://orgmode.org/worg/dev/org-syntax.html. It is not an official
    document but there is not a more official. I was pointed to that document
    asking on the mailinglist.

    Development note:
        A node (with "d") in OrgRoam is an entity with an ID. A note (with
        "t") is something more broad.

        Escaping HTML entities (e.g. "&" into "&amp;") is done here in that
        class (see Issue #47). This is not a clear solution because HTML
        related operations should be done in `Exporter`. The `Content` class
        should be independent from output format. In consequence the current
        solution is not for ever and will be refactored once.
    """

    input_dir: pathlib.Path
    """Input directory where the Org-roam content was read from"""

    nodes: dict = {}
    """List of all Org-roam nodes in `Content` format"""

    heading_ids: dict = {}
    """Org-IDs of headings (subtrees)"""

    IDX_ALL = 'all'

    IDX_FILETAG = 'filetag'

    IDX_NO_TAG = '(no tags)'

    index_data: dict = {
        IDX_ALL: defaultdict(list),
        IDX_FILETAG: defaultdict(lambda: defaultdict(list))
    }
    """Nested dictionary containing glossary like data."""

    META_FIELDS_INHERIT = ['date', 'filetags']
    """See `inherit_meta_fields_from()`. """

    __slots__ = (
        '_orgids_linking',
        'filename',
        '_meta',
        'title_org',
        'title_runs',
        '_elements',
        # '_e',
    )

    def __init__(self,
                 body_lines: Iterable[str],
                 filename: str,
                 title_aliases: Iterable[str] = None,
                 anchor_first_heading: bool = False):
        """Content ctor."""

        if title_aliases is None:
            title_aliases = []
        #
        self._orgids_linking = []

        # Name of the original Org-roam file
        self.filename = filename

        # Node title
        self.title_org = '[no title]'
        self.title_runs = textruns.cut_into_runs(self.title_org)

        # Meta data about that Org-roam node
        self._meta = {}

        # Title aliases (from :ROAM_ALIASES)
        if title_aliases:
            self._meta['aliases_org'] = []
            self._meta['aliases_runs'] = []
            for one_alias in title_aliases:
                self._meta['aliases_org'].append(one_alias)
                self._meta['aliases_runs'].append(
                    textruns.cut_into_runs(one_alias))

        # list of content elements
        self._elements = []

        # # the current active element
        # self._e = None

        self.parse_lines(body_lines)

        # When this Content instance represent a subheading with its own OrgID
        if anchor_first_heading:
            if not isinstance(self._elements[0], elements.Heading):
                raise TypeError('First element need to be a heading.')

            self._elements[0].generate_anchor()

    def _on_link_created(self, linkrun):
        if linkrun.kind == textruns.Link.Kind.ORGID:
            self._orgids_linking.append(linkrun.target)

    def __getitem__(self, idx: int):
        return self._elements[idx]

    def __len__(self):
        return len(self._elements)

    def add_subheading_content(self, content):
        """Add child node to this one.
        """

        content.inherit_meta_fields_from(self)

        # Content element, subtree/heading with own OrgId
        # self._finish_current_element()
        # self._e = content
        self._elements.append(content)
        # self._finish_current_element()

    def inherit_meta_fields_from(self, content: Node):
        """Some meta fields from ``content`` are copied into the own.

        Which fields are copied is specified by `META_FIELDS_INHERIT'.

        Args:
            content: The content object to copy from.
        """

        for field in Node.META_FIELDS_INHERIT:
            try:
                val = content._meta[field]  # pylint: disable=protected-access
            except KeyError:
                pass
            else:
                own_val = self._meta.get(field, None)

                # just copy
                if own_val is None:
                    self._meta[field] = val
                    continue

                if not isinstance(own_val, list):
                    own_val = [own_val]

                if not isinstance(val, list):
                    val = [val]

                self._meta[field].extend(val)

    @classmethod
    def add_all_backlinks(cls,
                          backlinks: Dict[Hashable, Dict[Hashable, Iterable]]):
        """Create backlinks elements for all nodes.
        """

        for orgid in backlinks:
            if orgid in cls.nodes:
                cls.nodes[orgid].add_backlinks(backlinks[orgid])

    def add_backlinks(self, backlinks: Dict[Hashable, Iterable]):
        """Create backlink element for that node."""

        # self._finish_current_element()

        for orgid in backlinks:
            # create org-file line
            item_string = f'- [[id:{orgid}][{Node.nodes[orgid].title_org}]]'

            # create backlink element if needed
            if (not self._elements
                    or not isinstance(self._elements[-1], elements.Backlinks)):
                self._elements.append(
                    elements.Backlinks(item=item_string))

            else:
                self._elements[-1].add_item(item_string)

            # subheadings?
            for subheading_id in backlinks[orgid]:
                idx = Node.heading_ids[subheading_id][1]
                subheading_title = str(Node.nodes[orgid][idx][0])

                # create org-file line
                item_string = f'  - [[id:{subheading_id}][{subheading_title}]]'

                self._elements[-1].add_item(item_string)

        # self._finish_current_element()

    def parse_lines(self, to_parse: list):
        """Create Content elements based on text lines.

        Classify a list of lines and create Content elements
        (e.g. Content.Paragraph) of it.
        To differentiate a list item from a head when an asterix is used as
        bullet it is important to check for heading first.
        """
        with textruns.Link.event_link_created.register_temp(
                self._on_link_created):

            meta, elem = parser.OrgContentParser(to_parse).result()

            self._meta = {**self._meta, **meta}
            self._elements.extend(elem)

            try:
                # Title as runs is used in the <h1> element.
                # But when handling (back)links it might be easier using the
                # original org-syntax not parsed into runs.
                self.title_org = self._meta.pop('title')
                self.title_runs = textruns.cut_into_runs(self.title_org)
            except KeyError:
                pass

    def get_orgid_links(self) -> list:
        """Give a list of OrgID's this content instance is linking to.

        Be aware that subheading content objects are ignored.

        Returns:
            List of OrgID's
        """

        return self._orgids_linking
