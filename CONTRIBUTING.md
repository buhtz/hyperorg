<!---
# SPDX-FileCopyrightText: © 2024 Christian BUHTZ <c.buhtz@posteo.jp>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# This file is part of the program "Hyperorg" which is released under GNU
# General Public License v3 (GPLv3).
# See folder LICENSES or go to <https://www.gnu.org/licenses/#GPL>.
-->
# How to contribute to _Hyperog_

😊 **Thanks for taking the time to contribute!**

The maintenance team will welcome all types of contributions. No contribution
will be rejected just because it doesn't fit to our quality standards,
guidelines or rules. We are willing to guide and mentorship new contributors
through the process.

<sub>March 2024</sub>

## Table of Contents
<!-- TOC start (generated with https://github.com/derlin/bitdowntoc) -->
- [How to submit changes and report bugs?](#how-to-submit-changes-and-report-bugs)
- [Setup environment, installing and testing](#setup-environment-installing-and-testing)
- [Recommendations & Best practices](#recommendations-best-practices)
- [Branching model](#branching-model)
- [Licensing of contributed material](#licensing-of-contributed-material)
<!-- TOC end -->

# How to submit changes and report bugs?
These external resources explain how to accomplishing various tasks through contribution:

- [Opening an issue or bug report](https://opensource.guide/how-to-contribute/#opening-an-issue)
- [Opening a pull request](https://opensource.guide/how-to-contribute/#opening-a-pull-request)

# Setup environment, installing and testing

Fork the repository and clone it to your local machine.
Please **always make a new branch** when preparing a _Pull Request_ ("PR") and
**baseline it on** the repositories default branch `latest`.

    $ git clone git@codeberg.org:YOUR_USERNAME/hyperorg.git
    $ cd hyperorg
    $ git checkout --branch mynewfeature

Create a virtual environment, activate it and install _Hyperorg_ (dependencies
should be handled automatically by `pip`):

    $ python3 -m venv .venv
    (.venv) $ source venv/bin/activate
    (.venv) $ python3 -m pip install --editable .[develop]

Run the test suite using `unittest` or `pytest` with one of the two following
commands:

    (.venv) $ python3 -m unittest
    (.venv) $ pytest

Create and add your modifications and push it to your remote repository:

    (.venv) $ edit myfancynewfeature.py
    (.venv) $ git add myfancynewfeature.py
    (.venv) $ git commit -am 'feat: fancy colors'
    (.venv) $ git push

Go to your repository at [Codeberg.org](https://codeberg.org) and open a _Pull
Request_.

# Branching model

This project utilizes the
[OneFlow](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow)
branching model, initially outlined by
[Adam Ruka](https://github.com/skinny85).
Its key aspects and rules are straightforward: there exists only one
branch (named `latest` in this repository), and stable releases are denoted by
annotated tags using the version number (e.g. `v0.1.1`).

# Recommendations & Best practices

Please take the following best practices into account if possible (to reduce
the work load of the maintainers and to increase the chance that your pull
request is accepted):

 - Follow [PEP 8](https://peps.python.org/pep-0008/) as a minimal Style Guide
   for Python Code.
 - Follow [Google Style Guide](https://sphinxcontrib-napoleon.readthedocs.org/en/latest/example_google.html) for
   docstrings.
   See also [this example in PyDoctor's documentation](https://pydoctor.readthedocs.io/en/latest/docformat/google_demo/index.html).
 - Using automatic formatters like `black` is not recommended. Please **mention
   the use** of them when opening a pull request.
 - Run the test suite before you open a Pull Request.
 - Try to create new tests if appropriate. Use Python's regular `unittest`
   instead of `pytest`. If you know the difference please try follow the
   _Classical (aka Detroit) school_ instead of _London (aka mockist) school_.

# Licensing of contributed material

Information in mind as you contribute, that code, docs and other material
submitted to the project are considered licensed under the same terms (see
[LICENSES](LICENSES)) as the rest of the work. The project does use the
specifications from [REUSE Software](https://reuse.software) and
[SPDX](https://spdx.github.io/spdx-spec) to store license and copyright
information.

<sub>March 2024</sub>
